const mongoose = require('mongoose');

// Define the schema
const PatientSchema = new mongoose.Schema({
  id: Number,
  first_name: String,
  last_name: String,
  email: String,
  gender: String,
  phone_number: String,
  Appointment_id: mongoose.Schema.Types.ObjectId,
  Doctor_note: String,
  investigation_list: Array,
  date: String,
  total_price: String,
  tele:String,
  diagnostic_center_id: mongoose.Schema.Types.ObjectId,
});

// Create the model
const PatientDataModel = mongoose.model('appointments', PatientSchema);

// Export the model
module.exports = PatientDataModel;
