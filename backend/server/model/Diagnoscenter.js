const mongoose = require('mongoose')

const DiagnosticCenterSchema = new mongoose.Schema({
   centername:String,
   email:String,
   password:String,
   address:String,
   credit: Number,
   
})

const DiagnosticCenterModel = mongoose.model("diagnostics", DiagnosticCenterSchema)

module.exports = DiagnosticCenterModel

