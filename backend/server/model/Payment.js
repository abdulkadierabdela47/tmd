const mongoose = require('mongoose');

const PaymentSchema = new mongoose.Schema({
  first_name: String,
  last_name: String,
  Appointment_id: String,
  investigation_list: Array,
  date: Date,
  total_price: Number,
  percentage_price:String,
  PatientId:mongoose.Schema.Types.ObjectId,
  diagnostic_center_id: mongoose.Schema.Types.ObjectId,
  status:String,

});

// Create the model
const PaymentModel = mongoose.model('Payment', PaymentSchema);

// Export the model
module.exports = PaymentModel;
