const mongose = require("mongoose");

const ResultSchema = mongose.Schema({
    filename:{
        type:String,
        required:true,
    },
    filepath:{
        type:String,
        required:true,


    },
});

const ResultModel = mongose.model("Result",ResultSchema);
module.exports = ResultModel;