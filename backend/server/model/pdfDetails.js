const mongoose = require("mongoose");

const PdfDetailsSchema = new mongoose.Schema(
  {
    pdf: String,
    title: String,
    first_name:String,
    last_name: String,
    patient_id:mongoose.Schema.Types.ObjectId,
    date: String,
    diagnostic_center_id: mongoose.Schema.Types.ObjectId,
  },
  { collection: "PdfDetails" }
);

mongoose.model("PdfDetails", PdfDetailsSchema);
