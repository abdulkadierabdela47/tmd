import mongoose from "mongoose";
// ,, ,,,
const UserSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  name: { type: String, required: true },
  phone: { type: Number },
  photo: { type: String },
  role: {
    type: String,
    enum: ["patient", "admin", "hr", "finance","diagnostic_center"],
    default: "patient",
  },
  gender: { type: String, enum: ["male", "female"] },
  bloodType: { type: String },
  allergies: { type: String },
  Woreda: { type: String },
  subCity: { type: String },
  city: { type: String },
  age: { type: String },
  appointments: [{ type: mongoose.Types.ObjectId, ref: "Appointment" }],
  diagnostic_center: [
    { type: mongoose.Types.ObjectId, ref: "diagnostic_center" },
  ],
});

export default mongoose.model("User", UserSchema);
