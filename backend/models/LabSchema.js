import mongoose, { Mongoose } from "mongoose";

// Define the schema
const PatientSchema = new mongoose.Schema({
  id: Number,
  patient_id:mongoose.Schema.Types.ObjectId,
  first_name: String,
  last_name: String,
  email: String,
  gender: String,
  phone_number: String,
  Appointment_id: String,
  Doctor_note: String,
  investigation_list: Array,
  date: String,
  total_price: String,
  tele:String,
  diagnostic_center_id: mongoose.Schema.Types.ObjectId,
  doctors_id:mongoose.Schema.Types.ObjectId,
});

// Create the model
const Patient = mongoose.model("appointments", PatientSchema);

export default Patient;







