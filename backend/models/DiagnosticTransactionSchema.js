import mongoose from "mongoose";

const DiagnosticTransactionSchema = new mongoose.Schema(
  {
    date: { type: Date },
    isApproved: {
      type: String,
      enum: ["paid", "unpaid"],
      default: "paid",
    },

    credit: { type: Number },
    totalCredit: { type: Number },
    diagnostic: {
      type: mongoose.Types.ObjectId,
      ref: "Diagnostic",
    },
    user: {
      type: mongoose.Types.ObjectId,
      ref: "User",
    },
  },
  { timestamps: true }
);

export default mongoose.model(
  "DiagnosticTransaction",
  DiagnosticTransactionSchema
);
