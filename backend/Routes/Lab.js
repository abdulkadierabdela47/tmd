
import express from "express";
import { authenticate, restrict } from "./../auth/verifyToken.js";
import { registerTests } from "../Controllers/LabController.js";

const router = express.Router({ mergeParams: true });

router.route("/")
  .post(authenticate, restrict(["doctor"]), registerTests)
  .post(registerTests);

export default router;