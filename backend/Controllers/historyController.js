import History from "../models/HistorySchema.js";
import Doctor from "../models/DoctorSchema.js";
import User from "../models/UserSchema.js";

export const createHistory = async (req, res) => {
  if (!req.body.user) req.body.user = req.params.doctorId;
  if (!req.body.doctor) req.body.doctor = req.userId;
  //   const { time, date, doctor, user, ticketPrice } = req.body;
  // let newBook =null;
  // if (date && time) {
  // }
  const newBook = new History(req.body);
  console.log(req.body);
  try {
    const savedBooked = await newBook.save();
    await Doctor.findByIdAndUpdate(req.body.user, {
      $push: { diagnostic_center: savedBooked._id },
    });
    await User.findByIdAndUpdate(req.body.doctor, {
      $push: { diagnostic_center: savedBooked._id },
    });
    res.status(200).json({ success: true, message: "Successfully Created " });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Error  " + req.body });
  }
};

export const getAllHistory = async (req, res) => {
  const id = req.params.id;
  const user1 = req.userId;

  try {
    const patients = await History.find({ user: id });
    res.status(200).json({
      success: true,
      messate: "Patient History ",
      data: patients,

    });
    // console.log(id);
  } catch (error) {
    res
      .status(404)
      .json({ success: false, message: "Failed to find patient history" });
  }
};











// Controller function to fetch a single history record by ID
export const getHistoryById = async (req, res) => {
  try {
    const { id } = req.params; // Extract the history ID from the request parameters
    const userId = req.user._id; // Extract the user's ID from the authenticated request

    // Query the database for the history record based on the provided ID and user ID
    const history = await History.findOne({ _id: id, user: userId });

    if (!history) {
      return res.status(404).json({ message: "History not found" });
    }

    res.status(200).json({ history });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server Error" });
  }
};
