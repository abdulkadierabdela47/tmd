
import Patient from "../models/LabSchema.js";

export const registerTests = async (req, res) => {
  const currentDate = new Date().toLocaleDateString('en-US', { month: '2-digit' ,day: '2-digit', year: 'numeric' }); // Get current date in the format: month/day/year

    try {
      const { first_name,diagnostic_center_id,doctors_id, age,email, gender, phone_number, investigation_list,  total_price, _id,patient_id, appointment_id  } = req.body;
      const patient = new Patient({
        _id,
        first_name,
        patient_id,
        email,
        age,
        gender,
        phone_number,
        investigation_list,
        total_price,
        diagnostic_center_id,
        date: currentDate,
        doctors_id,
      });
      await patient.save();
      res.status(201).json({ message: 'Data saved successfully' });
    } catch (error) {
      console.error('Error saving data:', error);
      res.status(500).json({ error: 'An error occurred while saving the data' });
    }
}
