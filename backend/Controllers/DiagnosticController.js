import User from "../models/UserSchema.js";
import Booking from "../models/BookingSchema.js";
import Doctor from "../models/DoctorSchema.js";
import Diagnostic from "../models/DiagnosticSchema.js";
import DiagnosticTransactionSchema from "../models/DiagnosticTransactionSchema.js";
import bcrypt from "bcryptjs";


export const updateDiagnostic = async (req, res) => {
  const id = req.params.id;
  try {
    const updateUser = await Diagnostic.findByIdAndUpdate(
      id,
      { $set: req.body },
      { new: true }
    );

    res.status(200).json({
      success: true,
      messate: "successfully Updated",
      data: updateUser,
    });
  } catch (error) {
    res.status(500).json({ success: false, message: "Failed to update" });
  }
};

export const deleteDiagnostic = async (req, res) => {
  const id = req.params.id;

  try {
    await Diagnostic.findByIdAndDelete(id);

    res.status(200).json({
      success: true,
      messate: "successfully deleted",
    });
  } catch (error) {
    res.status(500).json({ success: false, message: "Failed to delete" });
  }
};

export const getSingleDiagnosticUser = async (req, res) => {
  const id = req.params.id;

  try {
    const user = await Diagnostic.findById(id).select("-password");

    res.status(200).json({
      success: true,
      messate: "User Found",
      data: user,
    });
  } catch (error) {
    res.status(404).json({ success: false, message: "Failed to find user" });
  }
};

export const getAllDiagnostic = async (req, res) => {
  //   const id = req.params.id;

  try {
    const users = await Diagnostic.find({}).select("-password");

    res.status(200).json({
      success: true,
      messate: "All Users Found",
      data: users,

    });
  } catch (error) {
    res.status(404).json({ success: false, message: "Failed to find users" });
  }
};

export const getDiagnosticUserProfile = async (req, res) => {
  const userId = req.userId;

  try {
    const user = await Diagnostic.findById(userId);
    if (!user) {
      return res
        .status(404)
        .json({ success: false, message: "User not found" });
    }

    const { password, ...rest } = user._doc;
    res.status(200).json({
      success: true,
      message: "profile info is getting",
      data: { ...rest },
    });
  } catch (error) {
    res.status(404).json({ success: false, message: "Something went wrong" });
  }
};

export const getMyAppointmentsn = async (req, res) => {
  try {
    //retrive appointments from booking schema
    const bookings = await Booking.find({ user: req.userId });

    //extract doctor id
    const doctorId = bookings.map((el) => el.doctor.id);

    //retrieve docror using Dr. id
    const doctors = await Doctor.find({ _id: { $in: doctorId } });

    res.status(200).json({
      success: true,
      message: "Appointments are getting",
      data: doctors,
    });
  } catch (error) {
    res.status(404).json({ success: false, message: "Something went wrong" });
  }
};

export const registerDiagnostic = async (req, res) => {
  console.log(req.body);
  const userId = req.userId;

  const {
    companyName,
    email,
    password,
    username,
    role,
    photo,
    phone,
    city,
    credit,
    service,
    subCity,
    Woreda,
  } = req.body;

  try {
    let diagnostic = null;
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(password, salt);

    diagnostic = new Diagnostic({
      companyName,
      email,
      password: hashPassword,
      photo,
      phone,
      role,
      city,
      subCity,
      Woreda,
      username,
      credit,
      service,
    });

    const diagnosticSaved = await diagnostic.save();

    const trans = new DiagnosticTransactionSchema({
      diagnostic: diagnosticSaved._id,
      credit: credit,
      user: userId,
    });
    await trans.save();

    res
      .status(200)
      .json({ success: true, message: "Account Successfully created" });
  } catch (error) {
    res
      .status(500)
      .json({ success: false, message: "Error, Try again" + error });
  }
};

export const getDiagnosticTransaction = async (req, res) => {
  try {
    const users = await DiagnosticTransactionSchema.find({})
      .populate("diagnostic")
      .populate("user");

    res.status(200).json({
      success: true,
      messate: "All Transaction",
      data: users,
    
    }
    );
  } catch (error) {
    res
      .status(404)
      .json({ success: false, message: "Failed to find Transaction" });
  }
};



export const createCredit = async (req, res) => {
  const  userId = req.body.userId;
  //console.log(userId);

  const { totalCredit, credit ,tcredit } = req.body;

  try {
    // Create a new transaction
    const newTransaction = new DiagnosticTransactionSchema({
      diagnostic: userId,
      credit: credit,
      totalCredit: totalCredit,
      credit: tcredit,

    });



    // Save the transaction
    const savedTransaction = await newTransaction.save();

    // Update the credit in the Diagnostic model
    await Diagnostic.findByIdAndUpdate(userId, { credit: totalCredit });

    res.status(200).json({ success: true, message: "Credit updated", data: savedTransaction });

  } catch (error) {
    res.status(500).json({ success: false, message: error.message });
  }
};

// Route to handle service registration
export const addservices = async (req, res) => {
  const  userId = req.body.userId;

  try {
    const { userId, serviceName, servicePrice } = req.body;
   
    // Find the diagnostic center by userId and update the service array
    const diagnosticCenter = await Diagnostic.findById(userId);
    diagnosticCenter.service.push({ serviceName, servicePrice });
    await diagnosticCenter.save();
    res.status(200).json({ message: "Service added successfully" });
  } catch (error) {
    console.error("Error adding service:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};







